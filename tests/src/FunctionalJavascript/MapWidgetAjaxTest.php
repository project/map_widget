<?php

namespace Drupal\Tests\map_widget\FunctionalJavascript;

use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\entity_test\Entity\EntityTest;
use Drupal\FunctionalJavascriptTests\WebDriverTestBase;
use Drupal\Tests\system\Functional\Entity\Traits\EntityDefinitionTestTrait;

/**
 * Test widget with ajax behavior.
 *
 * @group map_widget
 */
class MapWidgetAjaxTest extends WebDriverTestBase {

  use EntityDefinitionTestTrait;

  /**
   * Modules to enable.
   *
   * @var array
   */
  protected static $modules = ['map_widget', 'entity_test'];

  /**
   * A user with permission to administer site configuration.
   *
   * @var \Drupal\user\UserInterface
   */
  protected $user;

  /**
   * Required setting.
   *
   * @var string
   */
  protected $defaultTheme = 'stark';

  /**
   * Container injected service.
   *
   * @var \Drupal\Core\State\State
   */
  protected $state;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->state = $this->container->get('state');
    // Create a field.
    $definition['map_test_field'] = BaseFieldDefinition::create('map')
      ->setLabel('Map Widget Test')
      ->setDisplayOptions(
        'view',
        [
          'region' => 'hidden',
        ]
      )
      ->setDisplayOptions(
        'form',
        [
          'type' => 'map_assoc_widget',
          'region' => 'content',
          'settings' => [
            'size' => '40',
            'key_placeholder' => 'Key placeholder for map_assoc_widget',
            'value_placeholder' => 'Value placeholder for map_assoc_widget',
          ],
          'weight' => 90,
        ]
      )
      ->setTranslatable(FALSE)
      ->setDescription(
        'Map Widget description'
      )
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', FALSE);
    $this->state->set(
      'entity_test.additional_base_field_definitions',
      $definition
    );
    $this->applyEntityUpdates('entity_test');
    $this->user = $this->drupalCreateUser(
      [
        'administer site configuration',
        'administer entity_test content',
      ]
    );
    $this->drupalLogin($this->user);
  }

  /**
   * Tests that the widget displays properly.
   */
  public function testMapWidgetAjax() {
    // Display creation form.
    $this->drupalGet('entity_test/add');
    $this->assertSession()->fieldValueEquals("map_test_field[value][0][key]", '');
    $this->assertSession()->fieldValueEquals("map_test_field[value][0][value]", '');
    $this->assertSession()->elementExists('css', 'input[name="map_test_field_0_add_more"]');
    // Ajax test.
    $button = $this->getSession()->getPage()->find('css', 'input[name="map_test_field_0_add_more"]');
    $button->click();
    $this->assertSession()->waitForElement('css', 'input[data-drupal-selector="edit-map-test-field-value-1-value"]');
    $this->assertSession()->elementExists('css', 'input[data-drupal-selector="edit-map-test-field-value-1-value"]');
    $button->click();
    $this->assertSession()->waitForElement('css', 'input[data-drupal-selector="edit-map-test-field-value-2-value"]');
    $this->assertSession()->elementExists('css', 'input[data-drupal-selector="edit-map-test-field-value-2-value"]');
    $button->click();
    $this->assertSession()->waitForElement('css', 'input[data-drupal-selector="edit-map-test-field-value-3-value"]');
    $this->assertSession()->elementExists('css', 'input[data-drupal-selector="edit-map-test-field-value-3-value"]');
    // Submit with some value.
    $keys = [$this->randomMachineName(), $this->randomMachineName()];
    $values = [$this->randomMachineName(), $this->randomMachineName()];
    $edit = [
      "map_test_field[value][0][key]" => $keys[0],
      "map_test_field[value][0][value]" => $values[0],
      "map_test_field[value][1][key]" => $keys[1],
      "map_test_field[value][1][value]" => $values[1],
    ];
    $this->submitForm($edit, 'Save');
    preg_match('|entity_test/manage/(\d+)|', $this->getUrl(), $match);
    $id = $match[1];
    $testEntity = EntityTest::load($id);
    $storedValue = $testEntity->get('map_test_field')->value;
    $this->assertTrue(is_array($storedValue), 'map_test_field does not contain an array');
    $this->assertEquals(count($storedValue), 2, 'Returned array does not have exactly two elements.');
    for ($index = 0; $index < 2; $index++) {
      $this->assertTrue(isset($storedValue[$keys[$index]]), "Test key $index not present in value array.");
      $this->assertEquals($storedValue[$keys[$index]], $values[$index], "The value stored for the test key $index in the MapItem does not match the test value.");
    }
    $this->assertSession()->fieldValueEquals('map_test_field[value][0][key]', $keys[0]);
    $this->assertSession()->fieldValueEquals('map_test_field[value][0][value]', $values[0]);
    $this->assertSession()->fieldValueEquals('map_test_field[value][1][key]', $keys[1]);
    $this->assertSession()->fieldValueEquals('map_test_field[value][1][value]', $values[1]);
  }

}
