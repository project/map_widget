<?php

namespace Drupal\Tests\map_widget\Functional;

use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\entity_test\Entity\EntityTest;
use Drupal\Tests\BrowserTestBase;
use Drupal\Tests\system\Functional\Entity\Traits\EntityDefinitionTestTrait;

/**
 * Test the actual widget for map_widget.
 *
 * @group map_widget
 */
class MapWidgetTest extends BrowserTestBase {

  use EntityDefinitionTestTrait;

  /**
   * Modules to enable.
   *
   * @var array
   */
  protected static $modules = ['map_widget', 'entity_test'];

  /**
   * A user with permission to administer site configuration.
   *
   * @var \Drupal\user\UserInterface
   */
  protected $user;

  /**
   * Required setting.
   *
   * @var string
   */
  protected $defaultTheme = 'stark';

  /**
   * Container injected service.
   *
   * @var \Drupal\Core\State\State
   */
  protected $state;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->state = $this->container->get('state');
    // Create a field.
    $definition['map_test_field'] = BaseFieldDefinition::create('map')
      ->setLabel('Map Widget Test')
      ->setDisplayOptions(
        'view',
        [
          'region' => 'hidden',
        ]
      )
      ->setDisplayOptions(
        'form',
        [
          'type' => 'map_assoc_widget',
          'region' => 'content',
          'settings' => [
            'size' => '40',
            'key_placeholder' => 'Key placeholder for map_assoc_widget',
            'value_placeholder' => 'Value placeholder for map_assoc_widget',
          ],
          'weight' => 90,
        ]
      )
      ->setTranslatable(FALSE)
      ->setDescription(
        'Map Widget description'
      )
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', FALSE);
    $this->state->set(
      'entity_test.additional_base_field_definitions',
      $definition
    );
    $this->applyEntityUpdates('entity_test');
    $this->user = $this->drupalCreateUser(
      [
        'administer site configuration',
        'administer entity_test content',
      ]
    );
    $this->drupalLogin($this->user);
  }

  /**
   * Tests that the widget displays properly.
   */
  public function testMapWidget() {
    // Display creation form.
    $this->drupalGet('entity_test/add');
    $this->assertSession()->fieldValueEquals("map_test_field[value][0][key]", '');
    $this->assertSession()->fieldValueEquals("map_test_field[value][0][value]", '');
    $this->assertSession()->responseContains('placeholder="Key placeholder for map_assoc_widget"');
    $this->assertSession()->responseContains('placeholder="Value placeholder for map_assoc_widget"');
    $this->assertSession()->responseContains('Map Widget description');

    // Submit with some value.
    $key = $this->randomMachineName();
    $value = $this->randomMachineName();
    $edit = [
      "map_test_field[value][0][key]" => $key,
      "map_test_field[value][0][value]" => $value,
    ];
    $this->submitForm($edit, 'Save');
    preg_match('|entity_test/manage/(\d+)|', $this->getUrl(), $match);
    $id = $match[1];
    $this->assertSession()->pageTextContains("entity_test $id has been created.");
    $testEntity = EntityTest::load($id);
    $storedValue = $testEntity->get('map_test_field')->value;
    $this->assertTrue(is_array($storedValue), 'map_test_field does not contain an array');
    $this->assertEquals(count($storedValue), 1, 'Returned array does not have exactly one element.');
    $this->assertTrue(isset($storedValue[$key]), 'Test key not present in value array.');
    $this->assertEquals($storedValue[$key], $value, 'The value stored for the test key in the MapItem does not match the test value.');
    $this->assertSession()->fieldValueEquals('map_test_field[value][0][key]', $key);
    $this->assertSession()->fieldValueEquals('map_test_field[value][0][value]', $value);
  }

}
