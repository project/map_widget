<?php

namespace Drupal\map_widget\Plugin\Field\FieldWidget;

use Drupal\Component\Utility\Html;
use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Field\Attribute\FieldWidget;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;

/**
 * Plugin implementation of the 'map_assoc_widget' widget.
 */
#[FieldWidget(
  id: 'map_assoc_widget',
  label: new TranslatableMarkup('Single on/off checkbox'),
  field_types: ['map'],
  multiple_values: TRUE,
)]
class AssociativeArrayWidget extends WidgetBase {

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    $settings = parent::defaultSettings();
    $settings['size'] = 60;
    $settings['key_placeholder'] = '';
    $settings['value_placeholder'] = '';
    return $settings;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $elements = [];

    $elements['size'] = [
      '#type' => 'number',
      '#title' => $this->t('Size of key and value input elements'),
      '#default_value' => $this->getSetting('size'),
      '#required' => TRUE,
      '#min' => 1,
    ];
    $elements['key_placeholder'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Placeholder for the key form field'),
      '#default_value' => $this->getSetting('key_placeholder'),
    ];
    $elements['value_placeholder'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Placeholder for the value form field'),
      '#default_value' => $this->getSetting('value_placeholder'),
    ];
    return $elements;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = [];

    $summary[] = $this->t(
      'Key & value input size: @size',
      ['@size' => $this->getSetting('size')]
    );
    if (!empty($this->getSetting('key_placeholder'))) {
      $summary[] = $this->t(
        'Key placeholder: @placeholder',
        ['@placeholder' => $this->getSetting('key_placeholder')]
      );
    }
    if (!empty($this->getSetting('value_placeholder'))) {
      $summary[] = $this->t(
        'Value placeholder: @placeholder',
        ['@placeholder' => $this->getSetting('value_placeholder')]
      );
    }
    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function formElement(
    FieldItemListInterface $items,
    $delta,
    array $element,
    array &$form,
    FormStateInterface $form_state,
  ) {
    $required = $element['#required'] ?? FALSE;
    $field_name = $this->fieldDefinition->getName();
    $element['#field_name'] = $field_name;
    $value = $items->isEmpty() ? [] : $items[$delta]->value;
    $count = $this->initCount($element, $delta, $value, $form_state);
    $element['#theme_wrappers'] = ['form_element'];
    $element['value'] = $this->valueForm($value, $count, $required);
    // Add 'add more' button, if not working with a programmed form.
    if (!$form_state->isProgrammed()) {
      $parents = $element['#parents'] ?? [];
      $field_name = $items->getName();
      $id_prefix = implode('-', array_merge($parents, [$field_name], [$delta]));
      $wrapper_id = Html::getUniqueId($id_prefix . '-map-assoc-more-wrapper');
      $element['#prefix'] = '<div id="' . $wrapper_id . '">';
      $element['#suffix'] = '</div>';

      $element['add_more'] = [
        '#type' => 'submit',
        '#name' => strtr($id_prefix, '-', '_') . '_add_more',
        '#value' => $this->t('Add an entry'),
        '#attributes' => ['class' => ['field-add-more-submit']],
        '#limit_validation_errors' => [array_merge($parents, [$field_name])],
        '#submit' => [[static::class, 'addMorePairsSubmit']],
        '#ajax' => [
          'callback' => [static::class, 'addMorePairsAjax'],
          'wrapper' => $wrapper_id,
          'effect' => 'fade',
        ],
      ];
    }
    return $element;
  }

  /**
   * Helper function to build the value form element array.
   *
   * @param array $value
   *   The value.
   * @param int $count
   *   The number of array elements.
   * @param bool $required
   *   Is the value required?
   *
   * @return mixed[]
   *   The render array.
   */
  protected function valueForm(array $value, int $count, bool $required): array {
    return [
      '#type' => 'map_associative',
      '#default_value' => $value,
      '#key_placeholder' => $this->getSetting('key_placeholder'),
      '#value_placeholder' => $this->getSetting('value_placeholder'),
      '#size' => $this->getSetting('size'),
      '#count' => $count,
      '#required' => $required,
    ];
  }

  /**
   * Submission handler for the "Add another item" button.
   */
  public static function addMorePairsSubmit(
    array $form,
    FormStateInterface $form_state,
  ) {
    $button = $form_state->getTriggeringElement();
    // Go one level up in the form, and get this delta value.
    $element = NestedArray::getValue($form, array_slice($button['#array_parents'], 0, -1));

    $field_name = $element['#field_name'];
    $parents = $element['#field_parents'];
    $delta = $element['#delta'];
    // Increment the associative item count.
    $field_state = static::getWidgetState($parents, $field_name, $form_state);
    $field_state['map_assoc_count'][$delta]++;
    static::setWidgetState($parents, $field_name, $form_state, $field_state);
    $form_state->setRebuild();
  }

  /**
   * Ajax callback for the "Add another item" button.
   *
   * This returns the new page content to replace the page content made obsolete
   * by the form submission.
   */
  public static function addMorePairsAjax(
    array $form,
    FormStateInterface $form_state,
  ) {
    $button = $form_state->getTriggeringElement();

    // Go one level up in the form, and get this delta value.
    $element = NestedArray::getValue($form, array_slice($button['#array_parents'], 0, -1));

    // Add a DIV around this element.
    $element['#prefix'] = '<div class="ajax-new-content">' . ($element['#prefix'] ?? '');
    $element['#suffix'] = ($element['#suffix'] ?? '') . '</div>';
    return $element;
  }

  /**
   * Include the element count for this item in the field state.
   *
   * @param array $element
   *   The current field element.
   * @param int $delta
   *   The item delta in the field.
   * @param array $value
   *   The value of the item.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current form state.
   *
   * @return int
   *   The current delta count.
   */
  protected function initCount(
    array $element,
    int $delta,
    array $value,
    FormStateInterface $form_state,
  ) {
    $field_name = $element['#field_name'];
    $parents = $element['#field_parents'];

    // Set the array element count for this delta if not set.
    $field_state = static::getWidgetState($parents, $field_name, $form_state);
    $field_state['map_assoc_count'] = $field_state['map_assoc_count'] ?? [];
    if (!isset($field_state['map_assoc_count'][$delta])) {
      $count = count($value);
      $field_state['map_assoc_count'][$delta] = $count ? $count : 1;
    }
    static::setWidgetState($parents, $field_name, $form_state, $field_state);

    return $field_state['map_assoc_count'][$delta];
  }

  /**
   * {@inheritdoc}
   */
  public function massageFormValues(
    array $values,
    array $form,
    FormStateInterface $form_state,
  ) {
    // The correct value is stored in the #value property of the form element.
    // This value was set as the value in the element class.
    // Remove 'add_more' property.
    if (isset($values[0]['add_more'])) {
      unset($values[0]['add_more']);
    }
    return $values;
  }

}
