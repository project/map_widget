Copyright 2020 by Shawn P. Duncan.  This code is
released under the GNU General Public License.
Which means that it is free software; you can redistribute it and/or modify
it under the terms of the [GNU General Public License](http://www.gnu.org/licenses/gpl.html) as published by
the Free Software Foundation; either version 3 of the License, or (at
your option) any later version.
